import RPi.GPIO as GPIO
import time

# Initial setup
GPIO.setmode(GPIO.BOARD)
signal_0 = 0
signal_1 = 0
GPIO.setwarnings(False)

# Setup pin to send a logic HIGH signal to gripper
send_list = [8,18]
GPIO.setup(send_list, GPIO.OUT)

# Setup pin to check for logic HIGH returning from gripper
signal_check_pin_0 = 40
signal_check_pin_1 = 38
check_list = [40, 38]
GPIO.setup(check_list, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

# Set time and send signal
t_end = time.time() + 5
GPIO.output(send_list, GPIO.HIGH)

# Check if signal has been returned
while time.time() < t_end:
    if GPIO.input(signal_check_pin_0) and signal_0 == 0:
        print("Succes! Signal was received on I/O line 0")
        signal_0 = 1
    elif GPIO.input(signal_check_pin_1) and signal_1 == 0:
        print("Succes! Signal was received on I/O line 1")
        signal_1 = 1
    elif signal_0 == 1 and signal_1 == 1:
        print("Both Digital I/O lines work as they should")
        GPIO.cleanup()
        exit(0)

if signal_0 == 0:
    print("Failure, no signal was received in time on lino 0")
if signal_1 == 0:
    print("Failure, no signal was received in time on lino 1")

print("Failure, no signal was received in time")